import React from 'react';
import './App.css';
import Form from './components/form';
import Header from './components/Header';
import Demo from './components/navbar';


function App() {
  return (
    <div className="App">
      <Header />
      <Demo />
      <main>
        <Form />
      </main>
    </div>
  );
}

export default App;
